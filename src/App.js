import { useRef, useState, useEffect } from "react";

//import styles
import "./App.css";

// import mui modules
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import PauseIcon from "@mui/icons-material/Pause";

// import local commponents
import Carousel from "./components/Carousel";

// import mock data
import data from "./mock/mock.json";

import { calculatePlayerTime } from "./utils/global-filter";

function App() {
  const videoPlayer = useRef(null);
  const [playState, setPlayState] = useState("paused");
  const [time, settime] = useState("00:00");
  const [videoSlider, setVideoSlider] = useState(0);
  const [endVideo, setEndVideo] = useState(false);
  const [activeIndexVideo, setActiveIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      videoPlayer.current.onended = (event) => {
        setEndVideo(true);
      };
      if (videoPlayer.current.currentTime === 0) {
        settime(calculatePlayerTime(videoPlayer.current.duration));
      }
      if (videoPlayer.current.paused) return;
      settime(calculatePlayerTime(videoPlayer.current.currentTime));
      setVideoSlider(
        (videoPlayer.current.currentTime / videoPlayer.current.duration) * 100
      );
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  function playHandler() {
    if (videoPlayer.current.paused) {
      videoPlayer.current.play();
      setPlayState("playing");
    } else {
      videoPlayer.current.pause();
      setPlayState("paused");
    }
  }
  function onChangeUrl(index) {
    setActiveIndex(index);
    setEndVideo(false);
    setPlayState("paused");
    setVideoSlider(0);
  }

  function volumeChangeHandler(e) {
    videoPlayer.current.volume = +e.target.value / 10;
  }

  function videoSliderHandler(e) {
    videoPlayer.current.currentTime =
      (e.target.value * Math.floor(videoPlayer.current.duration)) / 100;
    settime(calculatePlayerTime(videoPlayer.current.currentTime));
    setVideoSlider(
      (videoPlayer.current.currentTime / videoPlayer.current.duration) * 100
    );
  }

  return (
    <div className="App">
      <section className="container">
        <div id="video_player">
          <video
            ref={videoPlayer}
            src={data.listOfVideo[activeIndexVideo].videoLink}
            id="main-video"
          ></video>
          {endVideo && (
            <div>
              {activeIndexVideo > 0 && (
                <video
                  onClick={() => onChangeUrl(activeIndexVideo - 1)}
                  src={data.listOfVideo[activeIndexVideo - 1].videoLink}
                  alt=""
                  className="video-overlay previous-video"
                />
              )}
              {activeIndexVideo + 1 < data.listOfVideo.length && (
                <video
                  onClick={() => onChangeUrl(activeIndexVideo + 1)}
                  src={data.listOfVideo[activeIndexVideo + 1].videoLink}
                  alt=""
                  className="video-overlay next-video"
                />
              )}
            </div>
          )}

          <div className="controls">
            <div className="controls-list">
              <div className="controls-left">
                <div className="volume-chooser">
                  <div className="volume-button">
                    <span>
                      <VolumeUpIcon />
                    </span>
                  </div>
                  <div className="volume-control">
                    <input
                      className="volume"
                      type="range"
                      max="10"
                      onChange={volumeChangeHandler}
                    />
                  </div>
                </div>
                {playState === "paused" ? (
                  <PlayArrowIcon className="play" onClick={playHandler} />
                ) : (
                  <PauseIcon className="play" onClick={playHandler} />
                )}
                <div className="timer">
                  <span className="current">{time}</span>
                  <input
                    type="range"
                    className="slider-bar"
                    aria-label="videoSlider"
                    value={videoSlider}
                    min={0}
                    max={100}
                    onChange={videoSliderHandler}
                  />
                </div>
              </div>
              <div className="controls-right">
                {data.listOfVideo[activeIndexVideo].title}
              </div>
            </div>
          </div>
        </div>
      </section>
      <Carousel changeUrl={onChangeUrl} data={data} />
    </div>
  );
}

export default App;
