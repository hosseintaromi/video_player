export const calculatePlayerTime = (timeInSecond) => {
  let data = "";
  if (timeInSecond == null) {
    return "";
  }
  let hours = Math.floor(timeInSecond / 3600);
  let minutes = Math.floor((timeInSecond - hours * 3600) / 60);
  let seconds = Math.ceil(timeInSecond - hours * 3600 - minutes * 60);
  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }
  data = (hours !== "00" ? hours + ":" : "") + minutes + ":" + seconds;
  if (data === "NaN:NaN:NaN") {
    data = "loading";
  }
  return data;
};
