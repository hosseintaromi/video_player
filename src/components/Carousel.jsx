import React from "react";
import "./Carousel.css";

export const Carousel = (props) => {
  function changeUrl(index) {
    props.changeUrl(index);
  }
  return (
    <div>
      <section>
        <div className="rt-container">
          <div className="col-rt-12">
            <div className="demo-container">
              <div className="carousel">
                <input
                  checked="checked"
                  className="carousel__activator"
                  id="carousel-slide-activator-1"
                  name="carousel"
                  type="radio"
                  readOnly
                />
                <input
                  className="carousel__activator"
                  id="carousel-slide-activator-2"
                  name="carousel"
                  type="radio"
                />
                <input
                  className="carousel__activator"
                  id="carousel-slide-activator-3"
                  name="carousel"
                  type="radio"
                />
                <div className="carousel__controls">
                  <label
                    className="carousel__control carousel__control--htmlForward"
                    htmlFor="carousel-slide-activator-2"
                  >
                    👉
                  </label>
                </div>
                <div className="carousel__controls">
                  <label
                    className="carousel__control carousel__control--backward"
                    htmlFor="carousel-slide-activator-1"
                  >
                    👈
                  </label>
                  <label
                    className="carousel__control carousel__control--htmlForward"
                    htmlFor="carousel-slide-activator-3"
                  >
                    👉
                  </label>
                </div>
                <div className="carousel__controls">
                  <label
                    className="carousel__control carousel__control--backward"
                    htmlFor="carousel-slide-activator-2"
                  >
                    👈
                  </label>
                </div>
                <div className="carousel__screen">
                  <div className="carousel__track">
                    {props.data.listOfVideo.map((item, index) => (
                      <div
                        key={index}
                        onClick={() => changeUrl(index)}
                        className="carousel__item carousel__item--desktop-in-3"
                      >
                        <div className="demo-content">
                          <video src={item.videoLink} alt="" />
                          <div>{item.title}</div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};
export default Carousel;
